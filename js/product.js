/* Extract id param in page url*/
// Object instance creation from URL constructor
let params = new URL(document.location).searchParams;
let id = params.get("id");
console.log(id);

// Select DOM elements
const productPageTitle = document.querySelector("center-title");
const productPageImg = document.querySelector(".product-zoom");
const productPageName = document.querySelector(".product-content-title");
const productPageDescription = document.querySelector(".product-content-desc");
const productPagePrice = document.querySelector(".product-price");

// Call functions
(function () { return getStorage(); })();
(function () { return getItems(); })();
(function () { return totalProductsNumber(); })();
(function () { return addToCart(); })();

/**
 * CALL API PRODUCT BY ID: Function for each product render
 * @param { String } id
 * @param { Object } apiResponse
 * @param { Object } error
 * @param { Object[] } apiDatas
 * @param { Array.<Object> } item
 * @return { Promise<Function> } fetch
 * @return { Promise.resolve<Array> } item
 * @return { Promise.reject<Error> } error or BadRequestError
 */

function getItems() {
  // Display 404 error if id lenght !== 24 or id is undefined
  if (id.length !== 24 || id == undefined) {
    check404Error();
  }
  // GET request with id as param
  fetch(`${apiUrl}/api/teddies/` + id)
    .then(function (apiResponse) {
      return apiResponse.json();
    })
    .catch((error) => {
      let alertContainer = document.querySelector(".alert-container");
      alertContainer.innerHTML =
        "Notre site internet nécessite que le javascript soit activé.<br /> Merci de vérifier la configuration de votre navigateur internet.";
      alertContainer.classList.add("alert-nojs");
    })
    .then(function (apiDatas) {
      // Map from api to html
      item = apiDatas;
      productPageName.innerHTML = item.name;
      productPageImg.src = item.imageUrl;
      productPageDescription.innerText = item.description;

      // Format price
      item.price = item.price / 100;
      productPagePrice.innerText = `Prix : ` + new Intl.NumberFormat("fr-FR", {
        style: "currency",
        currency: "EUR",
      }).format(item.price);

      // Options colors selection
      const colorUserChoice = document.getElementById("productOptions");
      const displayOptions = item.colors;
      // for while; display all colors options for each product
      for (let i = 0; i < displayOptions.length; i++) {
        let addProductOptions = document.createElement("option");
        addProductOptions.innerText = displayOptions[i];
        addProductOptions.value = displayOptions[i];
        colorUserChoice.appendChild(addProductOptions);
        colorUserChoice.classList.add("custom-select");
      }
    });
}


















































