/*
Project Name: P5 Orinoco e-commerce
Description: Orinoco e-commerce
Version: 1.0 - MPV
Author: Blas Pierre-Gérard
Email: pierreg.blas@gmail.com
Git repository: https://gitlab.com/pgb_oc/orinoco
GitLab page: https://pgb_oc.gitlab.io/orinoco/
Local server: nodejs localhost:3000
Demo server: https://pgb-orinoco.herokuapp.com/
Last modification: 18/10/2021
 
  ---------------------------
  FUNCTIONS.JS
  ---------------------------
  
  TABLE OF FUNCTIONS
  ---------------------------
   01. CART
   02. ORDER
   03. RENDER
*/

function getStorage() {
    // Init storage - JSON.parse -> convert JSON format to JS object
    return storage = JSON.parse(localStorage.getItem("products"));
}

/* ===================================================================================================== */
/* 01. CART                                                                                              */
/* ===================================================================================================== */

/**
 * ADD TO CART: Add product(s) in cart
 * @param { Object } storage
 * @param { Array } products
 * @return { Promise } Sweet alert promise
 * @return { Promise.resolve<Function> } addItem callback function
 * @return { Promise.reject } cancel
 */

function addToCart() {
    const addToCartButton = document.querySelector(".add-to-cart");
    const productsQuantity = document.querySelector("#productsQuantity");
    const colorUserChoice = document.querySelector('#productOptions');
    const confirmAddMessText = document.querySelector(".alert-added-confirmation-text");
    addToCartButton.addEventListener("click", (event) => {
        event.preventDefault();
        // Set values min max with condition
        if (productsQuantity.value > 0 && productsQuantity.value < 100) {
            // Cart storage
            var storage = localStorage.getItem("products");
            if (!storage) {// If storage not exist
                storage = {
                    products: [],
                }
            }
            else {
                storage = JSON.parse(storage)
            }

            /*Function add product*/
            const addItem = () => {
                // Add product with user options
                storage.products.push({
                    id: item._id,
                    name: item.name,
                    description: item.description,
                    imageUrl: item.imageUrl,
                    price: item.price,
                    quantity: parseFloat(productsQuantity.value),
                    colors: (colorUserChoice.value),
                })
                // Convert js object in JSON format in the "products" Key
                localStorage.setItem("products", JSON.stringify(storage));
            };
            // Sweet alert promise
            Swal.fire({
                title: 'Mise à jour du panier',
                text: `Vous allez ajouter ${productsQuantity.value} our(s) en peluche ${item.name} couleur: ${(document.querySelector("#productOptions").value)} à votre panier`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, je confirme !',
                cancelButtonText: 'Non, je ne veux pas !'
            })
                // Sweet alert promise state is resolved
                .then((result) => {
                    if (result.isConfirmed) {
                        addItem();
                        // Alert confirmation
                        Swal.fire(
                            'Mise à jour de votre panier.',
                            'Merci !')
                        window.location.href = "cart.html";
                    }
                })
        }
        else {
            confirmAddMessText.style.background = "transparent";
            confirmAddMessText.style.color = "white";
            confirmAddMessText.style.whiteSpace = "normal";
            confirmAddMessText.innerText = `La quantité doit être comprise entre 1 et 99`;
        }
    });//End of addToCart.addEventListener
}

/**
 * CART PRODUCTS TOTAL NUMBERS: numbers of products counter
 * @param { Array } totalProducts
 * @param { Object } storage
 * @param { Object } products
 * @param { Integer } quantity
 * @return { Integer } totalProductsInCart
 */

function totalProductsNumber() {
    // Init array
    let totalProducts = [];

    // DOM element select
    const totalNumbers = document.querySelector('#cart');

    // If cart is empty return 0 
    if (!storage || storage == 0) {
        var TotalCardEmpty = document.getElementById("cart");
        TotalCardEmpty.innerHTML = `0`;
    }
    else {
        // Quantity stockage for each product
        for (let n = 0; n < storage.products.length; n++) {
            let totalNumbers = storage.products[n].quantity;
            // Push quantity in var totalProducts
            totalProducts.push(totalNumbers);
        }

        // Numbers additions in totalProducts []
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        const totalProductsInCart = totalProducts.reduce(reducer, 0);
        console.log(totalProductsInCart);
        // Display total numbers counter
        totalNumbers.innerHTML = `${totalProductsInCart}`;

        // Set products total numbers in localstorage
        localStorage.setItem("totalNumber", totalProductsInCart);
    }
}

/**
 * CART PRODUCTS TOTAL PRICES: Total in cart calcul
 * @param { Array } totalCart
 * @param { Object } storage
 * @param { Object } products
 * @param { Integer } quantity
 * @param { Integer } price
 * @return { Integer } totalInCart
 */

function totalInCart() {
    // Display cart total
    let totalCart = [];

    // DOM select
    const cartTotal = document.querySelector('#cartTotal');

    // If cart is empty return 0,00 €
    if (!storage.products || storage.products == 0) {
        var TotalCardEmpty = document.getElementById("cartTotal");
        // Display in html
        TotalCardEmpty.innerHTML = `0,00 €`;
    }
    else {
        // Quantity stockage for each product
        for (let t = 0; t < storage.products.length; t++) {
            let totalPrice = storage.products[t].price * storage.products[t].quantity;
            // Push prices in var totalCart
            totalCart.push(totalPrice);
        }

        // Prices additions in arr totalCart
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        const totalInCart = totalCart.reduce(reducer, 0);

        /*// Display total with eur format
            cartTotal.innerHTML = `${totalInCart},00 €`;*/

        // Display total with eur format
        if (typeof Intl === "undefined" || typeof Intl === "NaN") {
            cartTotal.innerHTML = `0,00 €`;
        }
        else {
            cartTotal.innerHTML = 'Total :  ' + '&nbsp;&nbsp;' + new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
            }).format(totalInCart);

        }
        // Total localstorage stockage
        localStorage.setItem("total", totalInCart);
    }
}

/**
 * CART EMPTY ALL PRODUCTS: Clear the localStorage
 * @param { Object } storage
 * @return { Promise } Sweet alert promise
 * @return { Promise.resolve<Method> } .clear(storage)
 * @return { Promise.reject } cancel
 */

function emptyCart() {
    const eraseCartButton = document.querySelector("#eraseCart");
    eraseCartButton.addEventListener("click", (e) => {
        e.preventDefault();
        // Sweet alert integration
        Swal.fire({
            title: 'Etes-vous sur?',
            text: "de vouloir supprimer tous vos produits du panier ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, je confirme !',
            cancelButtonText: 'Non, je ne veux pas !',
            timer: 6000,
            timerProgressBar: true,
        })
            // Sweet alert promise state is resolved
            .then((result) => {
                if (result.isConfirmed) {
                    // Clear storage
                    localStorage.clear(storage);
                    // Alert confirmation
                    Swal.fire(
                        'Votre panier est maintenant vide !',
                        'Mise à jour de votre panier.'
                    )
                    // Redirection
                    window.location.href = "cart.html";
                }
            })
    })
}

/**
 * CART DELETE PRODUCT: Delete product from cart
 * @param { Object } storage
 * @param { Object } products
 * @return { Promise } Sweet alert promise
 * @return { Promise.resolve<Method> } storage.products.splice(p,1)
 * @return { Promise.reject } cancel
 */

function deleteProductCart() {
    const productDelete = document.querySelectorAll(".productDelete");
    for (let p in storage.products) {
        productDelete[p].addEventListener("click", (e) => {
            e.preventDefault();
            // Sweet alert promise
            Swal.fire({
                title: 'Etes-vous sur?',
                text: "de vouloir supprimer ce(ces) produit(s) ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, je confirme !',
                cancelButtonText: 'Non, je ne veux pas !',
                timer: 5000,
                timerProgressBar: true,
            })
                // Sweet alert promise state is resolved
                .then((result) => {
                    if (result.isConfirmed) {
                        // Delete the product     
                        storage.products.splice(p, 1);
                        // Alert confirmation
                        Swal.fire(
                            'Produit(s) supprimé(s) !',
                            'Mise à jour de votre panier.'
                        )
                        // Save in LS
                        localStorage.setItem("products", JSON.stringify(storage));
                        // Alert product deleted
                        //alert ("Ce produit vient d'être supprimer de votre panier");
                        // Redirection
                        window.location.href = "cart.html";
                    }
                })
        })
    }
}

/**
 * CART PRODUCT INCREMENT: Add product on click
 * @param { Object } storage
 * @param { Object } products
 * @param { Object } quantity
 * @return { Promise } Sweet alert integration
 * @return { Promise.resolve<Object> } storage.products[index]quantity++
 * @return { Promise.reject } cancel
 */

function productIncrement() {
    const addQuantityProduct = document.querySelectorAll(".product-increment");
    for (let q in storage.products) {
        addQuantityProduct[q].addEventListener("click", (e) => {
            e.preventDefault();
            // Sweet alert with timer integration
            Swal.fire({
                title: 'Mise à jour quantité en cours...',
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                }
            })
                // Sweet alert promise state is resolved
                .then((result) => {
                    // Increment product quantity
                    storage.products[q].quantity++;
                    // Save in LS
                    localStorage.setItem("products", JSON.stringify(storage));
                    // Alert product increment
                    //alert ("Quantité pour ce produit mise à jour");
                    // Redirection
                    window.location.href = "cart.html";
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                })
        })
    }
}

/**
 * CART PRODUCT DECREMENT: Delete product on click
 * @param { Object } storage
 * @param { Object } products
 * @param { Object } quantity
 * @return { Promise } Sweet alert timer callback
 * @return { Promise.resolve<Method> } storage.products.splice(d, 1)
 * @return { Promise.resolve<Object> } storage.products[index]quantity--
 * @return { Promise.reject } cancel
 */

function productDecrement() {
    const delQuantityProduct = document.querySelectorAll(".product-decrement");
    for (let d in storage.products) {
        //if(storage[d].quantity - 1 > 0){
        delQuantityProduct[d].addEventListener("click", (e) => {
            e.preventDefault();
            // Sweet alert promise with timer delay
            Swal.fire({
                title: 'Mise à jour quantité en cours...',
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                }
            })
                .then((result) => {
                    // Decrement product quantity
                    storage.products[d].quantity--;
                    // Delete product if quantity is <= 0
                    if (storage.products[d].quantity <= 0) {
                        storage.products.splice(d, 1);
                    }
                    // Save in LS
                    localStorage.setItem("products", JSON.stringify(storage));
                    // Alert product decrement
                    //alert ("Quantité pour ce produit mise à jour");
                    // Redirection
                    window.location.href = "cart.html";
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                })
        })
        //}
    }
}

/* ===================================================================================================== */
/* 02. ORDER                                                                                             */
/* ===================================================================================================== */

/**
 * ORDER POST: Send POST request to the backend
 * @param { Object } postHeader
 * @param { Object } apiResponse
 * @param { Object } apiDatas
 * @param { Object } error
 * @param { Object } order
 * @param { Object } orderId
 * @return { Promise<Function> } fetch
 * @return { Promise.resolve<Object> } order
 * @return { Promise.resolve<String> } orderId
 * @return { Promise.reject<Error> } error or BadRequestError
 */

function POST(order) {
    //const sendForm = document.querySelector("#sendOrder");
    // Set header of request
    const postHeader = {
        method: "POST",
        body: JSON.stringify(order),
        headers: { "Content-Type": "application/json" },
    };

    // Send request with postHeader param.
    // Block execution waiting the promise response
    fetch(`${apiUrl}/api/teddies/order`, postHeader)
        .then((apiDatas) => apiDatas.json())

        // Promise state is resolved
        .then((apiDatas) => {
            // Set Localstorages with order id and order data's
            localStorage.setItem("orderResult", JSON.stringify(apiDatas.orderId));
            localStorage.setItem("order", JSON.stringify(order));
        })
        // Promise state is rejected
        .catch((error) => {
            //Swal.fire('Form order request error' + error);
            console.log(error);
        });
    // Sweet alert promise with timer delay
    Swal.fire({
        title: 'Vérification de votre commande en cours...',
        timer: 1000,
        timerProgressBar: true,
        didOpen: () => {
            Swal.showLoading()
        }
    })
    //alert("Commande validée ! Merci.");
}



/**
 * SAVE ORDER STORAGE: Contact object and products id
 * @param { Array } productsInCart
 * @param { String } productsId
 * @param { Object } orderCart
 * @param { Object } contact
 * @param { Object[] } storage
 * @param { Array.<String> } products
 * @return { Function } POST(orderCart)
 */
function saveOrder() {
    // Listen the form button on click: Objects array with the orderer products "productsInCart"
    sendForm.addEventListener("click", function () {
        let storage = localStorage.getItem("products");
        storage = JSON.parse(storage);
        let productsInCart = []; // Post datas order array
        // Catch products id
        for (let x = 0; x < storage.products.length; x++) {
            let productsId = storage.products[x].id;
            // Push id's in contact.products
            productsInCart.push(productsId);
        }
        const orderCart = {
            contact: { // Contact object 
                firstName: firstName.value, //trim() method deleted random spaces
                lastName: lastName.value,
                address: address.value,
                postalCode: postalCode.value,
                city: city.value,
                email: email.value,
            },
            products: productsInCart, // products id
        };
        POST(orderCart); // call post function with contact object as param
        console.log(productsInCart);
    });
}

/**
 * CLEAR LOCAL STORAGE: Remove by key from localStorage
 * @param key
 * @param { Object } localStorage
 * @return { Object<Method> } .removeItem + key
 */

// Clear keys/values
function eraseLocalStorage(key) {
    localStorage.removeItem("products");
    localStorage.removeItem("orderResult");
    localStorage.removeItem("totalNumber");
    localStorage.removeItem("total");
    localStorage.removeItem("order");
    // Redirection
    window.location.href = "index.html";
}

/* ===================================================================================================== */
/* 03. RENDER                                                                                            */
/* ===================================================================================================== */

/**
 * CHECK 404 ERROR: Render when product id is wrong
 * @param { Object } window
 * @param { Method } addEventListener
 */

function check404Error() {
    window.addEventListener("error", (e) => {
        let alert404Container = document.querySelector(".alert-404-container");
        let alert404Btn = document.querySelector(".go-back-btn");
        alert404Container.classList.add("alert-404");
        alert404Container.innerHTML = `<p>Ce produit n\'existe pas ou n\'est plus disponible.</p>`;
        let productDisplay = document.querySelector(".product-infos-container");
        productDisplay.style.display = "none";
        let productImageDisplay = document.querySelector(".product-image");
        productImageDisplay.style.display = "none";
        let container404Display = document.querySelector(".display-product-position");
        container404Display.classList.add("col-l-center");
        alert404Container.appendChild(alert404Btn);
        alert404Btn.innerHTML = `<a href="index.html"><button class="button-404">Retourner au magasin</button></a>`;
    },
        true
    );
}

/**
 * RENDER PRODUCT CARD CROP: Description croped by regex
 * @param { String } Regex
 * @return { String } ...
 */

// Crop descriptions texts with regular expression 
function productDescCrop() {
    var descCrop = document.querySelectorAll(".card-content_desc");
    return descCrop = /(.{65})(.*)/;
}

/**
 * RENDER CART PRODUCT LIST CROP: Description croped by regex
 * @param { String } Regex
 * @return { String } ...
 */

function cartDescCrop() {
    var descCropCart = document.querySelectorAll(".card-content_desc");
    return descCropCart = /(.{124})(.*)/;
}
