// Call functions
(function () { return getStorage(); })();
(function () { return renderOrderConfirm(); })();
(function () { return totalProductsNumber(); })();

function renderOrderConfirm() {
    // Init Array
    var contact = [];
    //contact = storage;
   
    // Init stockages from post request
    const orderId = JSON.parse(localStorage.getItem("orderResult"));
    const orderDatas = JSON.parse(localStorage.getItem("order"));
    const totalPrice = JSON.parse(localStorage.getItem("total"));
    const confirmMess = document.querySelector("#confirmMess");

    // If storage is empty
    if (!storage) {
        // Redirection
        window.location.href = "index.html";
    }

    else {
        // DOM html selection
        const orderInfos = document.getElementById("orderInfos");
        const orderRefs = document.getElementById("orderRefs");

        // Map elements and html injection
        let userDataTitle = document.createElement("h2");
        orderInfos.appendChild(userDataTitle);
        orderRefs.appendChild(userDataTitle);
        userDataTitle.classList.add("product-content-title");
        userDataTitle.textContent = "Informations de commande : ";

        let userDataPrice = document.createElement("span");
        orderRefs.appendChild(userDataPrice);
        userDataPrice.classList.add("confirm-content-text");
        userDataPrice.innerHTML = 'Total panier : ' + '&nbsp;&nbsp;' + new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR",
        }).format(totalPrice);

        let userDataRef = document.createElement("span");
        orderRefs.appendChild(userDataRef);
        userDataRef.classList.add("confirm-content-text");
        userDataRef.textContent = "Votre référence commande : " + orderId;

        let userDataText = document.createElement("span");
        orderRefs.appendChild(userDataText);
        userDataText.classList.add("confirm-content-text");
        userDataText.innerHTML = `Article(s) commandé(s) :`;

        let userDataName = document.createElement("h2");
        orderInfos.appendChild(userDataName);
        userDataName.classList.add("product-content-title");
        userDataName.innerHTML = `Merci ${orderDatas.contact.firstName}, votre commande a bien été prise en compte.`;

        let userDataMailText = document.createElement("span");
        orderInfos.appendChild(userDataMailText);
        userDataMailText.classList.add("confirm-content-text");
        userDataMailText.textContent = "Un email de confirmation vient de vous être envoyé à :";

        let userDataMail = document.createElement("span");
        orderInfos.appendChild(userDataMail);
        userDataMail.classList.add("confirm-content-text");
        userDataMail.innerHTML = `${orderDatas.contact.email}`

        let userData = document.createElement("span");
        orderInfos.appendChild(userData);
        userData.classList.add("confirm-content-text");
        userData.textContent = "Votre commande sera envoyée à cette adresse :";

        let userDataAddress = document.createElement("span");
        orderInfos.appendChild(userDataAddress);
        userDataAddress.classList.add("confirm-content-text");
        userDataAddress.innerHTML = `Rue : ${orderDatas.contact.address}`;

        let userDataPostalCode = document.createElement("span");
        orderInfos.appendChild(userDataPostalCode);
        userDataPostalCode.classList.add("confirm-content-text");
        userDataPostalCode.innerHTML = `Code Postal : ${orderDatas.contact.postalCode}`;

        let userDataCity = document.createElement("span");
        orderInfos.appendChild(userDataCity);
        userDataCity.classList.add("confirm-content-text");
        userDataCity.innerHTML = `Ville : ${orderDatas.contact.city}`;

        let userDataList = document.createElement("ul");
        orderRefs.appendChild(userDataList);
        userDataList.classList.add("confirm-content-text");

        for (let i in storage.products) {
            /**
            * CALL API FOR ORDER: Async function for order render
            * @param { Object } apiResponse
            * @param { Object } error
            * @param { Object{} } orderDatas
            * @param { Object } storage
            * @param { Array.<Object> } products
            * @return { Promise<Function> } fetch
            * @return { Promise.resolve<Object> } storage.products
            * @return { Promise.reject<Error> } error or BadRequestError
            */

            function getOrder() {
                let productsList = document.createElement("li");
                productsList.classList.add("confirm-content-list");
                userDataList.appendChild(productsList);
                fetch(`${apiUrl}/api/teddies/` + orderDatas.products[i])
                    .then(async apiResponse => {
                        const response = await apiResponse.json()
                        api = response;
                        console.table(api);
                        productsList.textContent = "Produit : " + storage.products[i].name + " - " + "Couleur : " + storage.products[i].colors + " - " + "Quantité : " + storage.products[i].quantity + " - " + "Prix total : " + storage.products[i].price * storage.products[i].quantity + ",00€";
                    })
                    // Promise state is rejected
                    .catch((error) => {
                        let alertContainer = document.querySelector(".confirm-infos-container");
                        alertContainer.classList.add("alert-nojs");
                        alertContainer.innerHTML =
                            "Notre site internet nécessite que le javascript soit activé.<br /> Merci de vérifier la configuration de votre navigateur internet.";
                        console.error(error);
                    })
            }
            (function () { return getOrder(); })();
        }
    }

    // If have values in index 1 (2 products in cart)
    if (orderDatas.products[1]) {
        confirmMess.textContent = "Vos commandes sont en préparation et vous seront envoyées rapidement.";
    } else {
        confirmMess.textContent = "Votre commande est en préparation et vous sera envoyée rapidement.";
    }

    // Clear localstorage by user click action on elements
    let eraseDatas_01 = document.querySelector("#logoErase").addEventListener("click", function () {
        eraseLocalStorage(this);
    });
    let eraseDatas_02 = document.querySelector("#cartErase").addEventListener("click", function () {
        eraseLocalStorage(this);
    });
    let eraseDatas_03 = document.querySelector("#backAndErase").addEventListener("click", function () {
        eraseLocalStorage(this);
    });
}


