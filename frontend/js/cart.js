// Call functions
(function () { return getStorage(); })();
(function () { return renderCart(); })();
(function () { return totalInCart(); })();
(function () { return totalProductsNumber(); })();
(function () { return emptyCart(); })();
(function () { return deleteProductCart(); })();
(function () { return productIncrement(); })();
(function () { return productDecrement(); })();

/**
 * RENDER PRODUCTS IN CART: Products cart render
 * @param { Object[] } storage
 * @param { Array.<Object> } products
 * @return { Array.<Object> } storage.products
 */

function renderCart() {

  // Select DOM elements
  let productsCart = document.querySelector("#cartItems");
  let cartIsEmpty = document.querySelector("#emptyCart");
  // If localStorage exist display products
  if (storage) {
    productsCart.style.display = "visible";
    console.log(storage);
  }
  // If cart is empty
  if (!storage || storage == 0 || !storage.products || storage.products == 0) {
    cartIsEmpty.textContent = "Votre panier est actuellement vide";
    let formDisplay = document.querySelector(".hide-form");
    formDisplay.style.display = "none";
    let emptyCartDisplay = document.querySelector(".display-cart-position");
    emptyCartDisplay.classList.add("col-l-center");
    let emptyCartTotalDisplay = document.querySelector(".product-card-total-container");
    emptyCartTotalDisplay.style.display = "none";
    let emptyCartBtnDisplay = document.querySelector(".display-btn-position");
    emptyCartBtnDisplay.classList.add("col-l-center");
    let emptyCartTextDisplay = document.querySelector("#emptyCart");
    emptyCartTextDisplay = document.createElement("div");
    cartItems.appendChild(emptyCartTextDisplay);
    emptyCartTextDisplay.classList.add("product-card-empty");
    emptyCartTextDisplay.textContent = "Votre panier est actuellement vide";
    cartIsEmpty.style.display = "none";

  }

  // Redirection
  document.querySelector('#backtoHome').addEventListener("click", function () {
      window.location.href = "index.html";
  });

// Display data's in html for each product in cart
for (let p in storage.products) {
  let cartContainer = document.createElement("div");
  productsCart.appendChild(cartContainer);
  cartContainer.classList.add("cart-product-list-container");
  cartContainer.classList.add("card-animation", "animation-fadeinup", "fadeinup");

  let cartImgGrid = document.createElement("div");
  cartContainer.appendChild(cartImgGrid);
  cartImgGrid.classList.add("col-s-16", "col-m-4");

  let cartImgContainer = document.createElement("a");
  cartImgGrid.appendChild(cartImgContainer);
  cartImgContainer.href = `product.html?id=${storage.products[p].id}`;

  let cartImgList = document.createElement("div");
  cartImgContainer.appendChild(cartImgList);
  cartImgList.classList.add("product-list-img");

  let cartImg = document.createElement("img");
  cartImgList.appendChild(cartImg);
  cartImg.classList.add("product-list-zoom");
  cartImg.src = storage.products[p].imageUrl;
  cartImg.alt = `Ours en peluche ${storage.products[p].name}`;
  cartImg.height = 150;
  cartImg.width = 200;
  let cartContentGrid = document.createElement("div");
  cartContainer.appendChild(cartContentGrid);
  cartContentGrid.classList.add("col-s-16", "col-m-11", "col-l-12");

  let cartContentContainer = document.createElement("div");
  cartContentGrid.appendChild(cartContentContainer);
  cartContentContainer.classList.add("cart-product-list-content");

  let cartContentTitle = document.createElement("h3");
  cartContentContainer.appendChild(cartContentTitle);
  cartContentTitle.classList.add("product-content-list-title");
  cartContentTitle.innerHTML = `${storage.products[p].name}`;

  let cartContentDescription = document.createElement("p");
  cartContentContainer.appendChild(cartContentDescription);
  cartContentDescription.classList.add("product-content-desc");
  // Crop descriptions texts with regular expression
  let descCropCart = cartDescCrop();
  cartContentDescription.innerHTML = `${storage.products[p].description.replace(descCropCart, "$1...")}`;

  let cartContentPrice = document.createElement("div");
  cartContentContainer.appendChild(cartContentPrice);
  cartContentPrice.classList.add("product-content-price", "priceTotal");
  cartContentPrice.innerHTML = new Intl.NumberFormat("fr-FR", {
    style: "currency",
    currency: "EUR",
  }).format(storage.products[p].price * storage.products[p].quantity);

  let cartContentColor = document.createElement("div");
  cartContentContainer.appendChild(cartContentColor);
  cartContentColor.classList.add("product-content-color");
  cartContentColor.innerHTML = `Couleur : ${storage.products[p].colors}`;

  let cartContentUnitPrice = document.createElement("div");
  cartContentContainer.appendChild(cartContentUnitPrice);
  cartContentUnitPrice.classList.add("product-content-unit-price");
  cartContentUnitPrice.innerHTML = `P. U. : ${(new Intl.NumberFormat(
    "fr-FR",
    {
      style: "currency",
      currency: "EUR",
    }
  ).format(storage.products[p].price))}`;


  let cartWidgetsContainer = document.createElement("div");
  cartContentContainer.appendChild(cartWidgetsContainer);
  cartWidgetsContainer.classList.add("product-widgets-container");

  let cartWidgetsQuantity = document.createElement("div");
  cartContentContainer.appendChild(cartWidgetsQuantity);
  cartWidgetsQuantity.classList.add("product-widget-quantity");

  let cartWidgetDecrement = document.createElement("div");
  cartWidgetsQuantity.appendChild(cartWidgetDecrement);
  cartWidgetDecrement.classList.add("product-widget-decrement");

  let cartWidgetDecrementOnclick = document.createElement("div");
  cartWidgetDecrement.appendChild(cartWidgetDecrementOnclick);
  cartWidgetDecrementOnclick.classList.add("product-decrement");
  cartWidgetDecrementOnclick.textContent = "-";
  //cartWidgetDecrementOnclick.setAttribute('data-id' , '0');

  let cartWidgetQuantityContainer = document.createElement("div");
  cartWidgetsQuantity.appendChild(cartWidgetQuantityContainer);
  cartWidgetQuantityContainer.classList.add("product-widget-items-number");

  let cartWidgetProductQuantity = document.createElement("div");
  cartWidgetQuantityContainer.appendChild(cartWidgetProductQuantity);
  document.getElementById("productQuantity");
  cartWidgetProductQuantity.innerHTML = `${storage.products[p].quantity}`;

  let cartWidgetIncrement = document.createElement("div");
  cartWidgetsQuantity.appendChild(cartWidgetIncrement);
  cartWidgetIncrement.classList.add("product-widget-increment");

  let cartWidgetIncrementOnclick = document.createElement("div");
  cartWidgetIncrement.appendChild(cartWidgetIncrementOnclick);
  cartWidgetIncrementOnclick.classList.add("product-increment");
  cartWidgetIncrementOnclick.textContent = "+";
  //cartWidgetIncrementOnclick.setAttribute('data-id' , '1');

  let cartWidgetDeleteContainer = document.createElement("div");
  cartContentContainer.appendChild(cartWidgetDeleteContainer);
  cartWidgetDeleteContainer.classList.add("product-widget-delete");

  let cartWidgetDeleteButton = document.createElement("button");
  cartWidgetDeleteContainer.appendChild(cartWidgetDeleteButton);
  cartWidgetDeleteButton.classList.add("product-delete");
  cartWidgetDeleteButton.classList.add("productDelete");

  let cartWidgetDeleteIcon = document.createElement("i");
  cartWidgetDeleteButton.appendChild(cartWidgetDeleteIcon);
  cartWidgetDeleteIcon.classList.add("far", "fa-trash-alt", "icon-delete");

  let eraseCartButton = document.querySelector("#eraseCart");
  eraseCartButton.classList.add("card-animation", "animation-fadeinup", "fadeinup");
  eraseCartButton.innerHTML = `<a href="cart.html"><button>Vider le panier</button></a>`;
}
}