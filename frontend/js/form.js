//*** DOM elements selection ***//
const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const postalCode = document.querySelector("#postalCode");
const city = document.querySelector("#city");
const address = document.querySelector("#address");
const email = document.querySelector("#email");
const phone = document.querySelector("#phone");
const error = document.querySelector(".form-error");
const form = document.querySelector("#orderForm");
const sendForm = document.querySelector("#sendOrder");

// Call function
(function () { return saveOrder(); })();

// Listen the input firstName modification with callback function
form.firstName.addEventListener('change', function () {
    firstNameValidator(this); // add form.firstName as params    
});

// Listen the input lastName
form.lastName.addEventListener('change', function () {
    lastNameValidator(this);
});

// Listen the input address
form.address.addEventListener('change', function () {
    addressValidator(this);
});

// Listen the input postalCode
form.postalCode.addEventListener('change', function () {
    postalCodeValidator(this);
});

// Listen the input city
form.city.addEventListener('change', function () {
    cityValidator(this);
});

// Listen the input email
form.email.addEventListener('change', function () {
    emailValidator(this);
});

// Listen the input phone
form.phone.addEventListener('change', function () {
    phoneValidator(this);
});

// Listen the form submit
form.addEventListener('submit', function (e) {
    e.preventDefault();
    // if all inputs form have values
    if (firstNameValidator(form.firstName) && lastNameValidator(form.lastName)
        && postalCodeValidator(form.postalCode) && cityValidator(form.city)
        && addressValidator(form.address) && emailValidator(form.email)
        && phoneValidator(form.phone)) {
        error.textContent = "Informations validées"; // display confirm message
        form.submit(); // Send form
    }
    else {
        error.textContent = "Merci de vérifier vos informations"; // display error message
    }
});

// Listen inputs
form.addEventListener('input', function (e) {
    // Select all inputs in DOM
    const allInputs = document.getElementsByTagName("input");
    for (let f = 0; f < allInputs.length; f++) { // loop on all inputs values
        if (!allInputs[f].value) { // if not have value for each input
            e.preventDefault(); // Delete event by default
            allInputs[f].classList.add("alert-bg"); // add the alert class bg  
            allInputs[f].classList.add("alert-text-failed");
            error.textContent = "Veuillez renseigner tous les champs.";
            return false;
        }
    }
    if (allInputs.value) { // if have value for each input
        form.classList.remove("alert-bg"); // remove the alert class bg
        form.classList.remove("alert-text-failed"); // remove text color class
        form.classList.add("alert-text-success");
        return true;
    }
    else {
        form.classList.remove("alert-bg"); // remove the alert class bg
        form.classList.remove("alert-text-failed"); // remove text color class
        form.classList.add("alert-text-success");
        sendForm.removeAttribute("disabled", "");// delete disabled attribute for the validation button 
        return true;
    }
});

//*** Regex validation ***/

// First Name Validation
const firstNameValidator = function (inputFirstName) {
    // Regex creation for firstName
    let firstNameRegex = new RegExp(
        '^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{2,30}$',
        'g'//global flag for the regex
    );
    //*** firstName Input validation ***//
    let firstNameTest = firstNameRegex.test(inputFirstName.value);
    let error_01 = document.querySelector("#error_01");

    console.log(firstNameTest);
    // if condition is true
    if (firstNameTest) { // or (firstNameRegex.test(inputFirstName.value)) and delete firstNameTest stockage var
        firstName.classList.remove("alert-bg");
        firstName.classList.remove("alert-text-failed");
        firstName.classList.add("alert-text-success");
        error_01.textContent = "Votre nom est valide."; // display message
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_01.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        firstName.classList.add("alert-bg"); // add the alert class bg
        error_01.textContent = "Erreur: Le champ Nom est invalide, caractères autorisés: (A-Z) (a-z) (' space -).";
        firstName.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_01.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// Last Name Validation
const lastNameValidator = function (inputLastName) {
    // Regex creation for lastName
    let lastNameRegex = new RegExp(
        '^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{2,30}$',
        'g'//global flag for the regex
    );
    //*** lastName Input validation ***//
    let lastNameTest = lastNameRegex.test(inputLastName.value);
    let error_02 = document.querySelector("#error_02");

    // if condition is true
    if (lastNameTest) {
        lastName.classList.remove("alert-bg");
        lastName.classList.remove("alert-text-failed");
        lastName.classList.add("alert-text-success");
        error_02.textContent = "Votre prénom est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_02.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        lastName.classList.add("alert-bg"); // add the alert class bg
        error_02.textContent = "Erreur: Le champ Prénom doit comporter entre 2 et 30 caractères.";
        lastName.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_02.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// address Validation
const addressValidator = function (inputAddress) {
    // Regex creation for address
    let addressRegex = new RegExp(
        '^[a-zA-Z0-9-,_ ]{5,50}$',
        'g'//global flag for the regex
    );
    //*** address Input validation ***//
    let addressTest = addressRegex.test(inputAddress.value);
    let error_03 = document.querySelector("#error_03");

    // if condition is true
    if (addressTest) {
        address.classList.remove("alert-bg");
        address.classList.remove("alert-text-failed");
        address.classList.add("alert-text-success");
        error_03.textContent = "Votre adresse est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_03.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        address.classList.add("alert-bg"); // add the alert class bg
        error_03.textContent = "Erreur: Le champ Adresse doit comporter entre 5 et 50 caractères.";
        address.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_03.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// postalCode Validation
const postalCodeValidator = function (inputPostal) {
    // Regex creation for postal
    let postalRegex = new RegExp(
        '^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$',
        'g'//global flag for the regex
    );
    //*** postal Input validation ***//
    let postalTest = postalRegex.test(inputPostal.value);
    let error_04 = document.querySelector("#error_04");

    // if condition is true
    if (postalTest) {
        postalCode.classList.remove("alert-bg");
        postalCode.classList.remove("alert-text-failed");
        postalCode.classList.add("alert-text-success");
        error_04.textContent = "Votre code postal est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        formValidIcon.classList.add("form-icon-container");
        error_04.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition is not true
        postalCode.classList.add("alert-bg"); // add the alert class bg
        error_04.textContent = "Erreur: Le champ Code postal doit être du type ex: 59000.";
        postalCode.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_04.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// city Validation
const cityValidator = function (inputCity) {
    // Regex creation for city
    let cityRegex = new RegExp(
        '^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{2,30}$',
        'g'//global flag for the regex
    );
    //*** city Input validation ***//
    let cityTest = cityRegex.test(inputCity.value);
    let error_05 = document.querySelector("#error_05");

    // if condition is true
    if (cityTest) {
        city.classList.remove("alert-bg");
        city.classList.remove("alert-text-failed");
        city.classList.add("alert-text-success");
        error_05.textContent = "Votre Ville est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_05.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        city.classList.add("alert-bg"); // add the alert class bg
        error_05.textContent = "Erreur: Le champ Ville doit comporter entre 2 et 30 caractères.";
        city.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_05.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// email Validation
const emailValidator = function (inputEmail) {
    // Regex creation for email
    let emailRegex = new RegExp(
        '^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$',
        'g'//global flag for the regex
    );
    //*** email Input validation ***//
    let emailTest = emailRegex.test(inputEmail.value);
    let error_06 = document.querySelector("#error_06");

    // if condition is true
    if (emailTest) {
        email.classList.remove("alert-bg");
        email.classList.remove("alert-text-failed");
        email.classList.add("alert-text-success");
        error_06.textContent = "Votre email est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_06.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        email.classList.add("alert-bg"); // add the alert class bg
        error_06.textContent = "Erreur: Le champ Email doit être du type: exemple@gmail.com";
        email.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_06.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}

// phone Validation
const phoneValidator = function (inputPhone) {
    // Regex creation for phone
    let phoneRegex = new RegExp(/^[+](\d{3})\)?(\d{3})(\d{5,6})$|^(\d{10,10})$/);
    //*** phone Input validation ***//
    let phoneTest = phoneRegex.test(inputPhone.value);
    let error_07 = document.querySelector("#error_07");

    // if condition is true
    if (phoneTest) {
        phone.classList.remove("alert-bg");
        phone.classList.remove("alert-text-failed");
        phone.classList.add("alert-text-success");
        error_07.textContent = "Votre Téléphone est valide.";
        let formValidIcon = document.createElement("i");
        formValidIcon.classList.add("form-icon-container");
        error_07.appendChild(formValidIcon);
        formValidIcon.classList.add("fas", "fa-check")
        return true;
    }
    else {// if condition not true
        phone.classList.add("alert-bg"); // add the alert class bg
        error_07.textContent = "Erreur: Le Téléphone doit être du type: 0612345678 ou +33(0)612345678";
        phone.classList.add("alert-text-failed");
        let formInvalidIcon = document.createElement("i");
        formInvalidIcon.classList.add("form-invalid-icon-container");
        error_07.appendChild(formInvalidIcon);
        formInvalidIcon.classList.add("fas", "fa-exclamation");
        return false;
    }
}
