
// Call functions with anon autoinvocated function
(function () { return getStorage(); })();
(function () { return getProducts(); })();
(function () { return totalProductsNumber(); })();

/**
 * CALL API FOR PRODUCTS: Async function for products render
 * @param { Object } apiResponse
 * @param { Object } error
 * @param { Object[] } products
 * @param { Array.<Object> } item
 * @return { Promise<Function> } fetch
 * @return { Promise.resolve<Array> } item
 * @return { Promise.reject<Error> } error or BadRequestError
 */

async function getProducts() {
  let item;
  // Block execution waiting the promise response
  await fetch(`${apiUrl}/api/teddies`, document.querySelector("main"))
    .then(function (apiResponse) {
      return apiResponse.json();
    })
    // Promise state is rejected
    .catch((error) => {
      let alertContainer = document.querySelector(".alert-container");
      alertContainer.innerHTML =
        "Notre site internet nécessite que le javascript soit activé.<br /> Merci de vérifier la configuration de votre navigateur internet.";
      alertContainer.classList.add("alert-nojs");
      console.error(error);
    })
    // Promise state is resolved
    .then(function (item) {
      // Clone element for security
      //const templateElt = document.getElementById("templateHome")
      //const cloneElt = document.importNode(templateElt.contentEditable, true)
      //document.getElementById("main").appendChild(cloneElt)

      // API data -> variable stockage
      const products = item;
      console.log(item);// Check in console API results
      // Display the product data's for each product
      for (let i in products) {
        //parentNode.appendChild(childNode);
        let itemContainer = document.createElement("div");
        document.querySelector(".products-container").appendChild(itemContainer);
        itemContainer.classList.add("card-container", "card-animation", "animation-fadeinup", "fadeinup");

        let cardContainer = document.createElement("a");
        itemContainer.appendChild(cardContainer);
        cardContainer.href = `product.html?id=${item[i]._id}`;
        //cloneElt.querySelector(".card-link-activities").href = 'product.html?id=' + item[i]._id
        //cardContainer.classList.add("card-link-activities");
        let productCard = document.createElement("div");
        cardContainer.appendChild(productCard);
        productCard.classList.add("products-card");
        document.getElementById("productCard${i}");

        let productCardImg = document.createElement("div");
        productCard.appendChild(productCardImg);
        productCardImg.classList.add("products-card-img");

        let productCardImage = document.createElement("div");
        productCardImg.appendChild(productCardImage);
        productCardImage.classList.add("products-img_image");

        let productImage = document.createElement("img");
        productCardImage.appendChild(productImage);
        productImage.classList.add("products-zoom-adjustment");
        productImage.src = item[i].imageUrl;

        let productAlert = document.createElement("div");
        productCardImg.appendChild(productAlert);
        productAlert.classList.add("products-img_alert-new");

        let productAlertText = document.createElement("div");
        productAlert.appendChild(productAlertText);
        productAlertText.classList.add("alert-text");
        productAlertText.innerHTML = `Nouveau`;

        let productImgHover = document.createElement("div");
        productCardImg.appendChild(productImgHover);
        productImgHover.classList.add("products-img_hover");

        let productImgName = document.createElement("div");
        productCardImg.appendChild(productImgName);
        productImgName.classList.add("products-img_name");

        let productSeeMore = document.createElement("div");
        productImgName.appendChild(productSeeMore);
        productSeeMore.classList.add("see-more");
        document.getElementById("teddy${i}");
        productSeeMore.innerHTML = `Voir la page produit`;

        let productCardContent = document.createElement("div");
        productCardImg.appendChild(productCardContent);
        productCardContent.classList.add("products-card-content");

        let cardContentTitle = document.createElement("h3");
        productCardContent.appendChild(cardContentTitle);
        cardContentTitle.classList.add("card-content_title");
        cardContentTitle.innerHTML = `${item[i].name}`

        let cardContentDesc = document.createElement("div");
        productCardContent.appendChild(cardContentDesc);
        cardContentDesc.classList.add("card-content_desc");
        // Crop descriptions texts with regular expression
        let descCrop = productDescCrop();
        cardContentDesc.innerHTML = `${item[i].description.replace(descCrop, "$1...")}`;

        let cardContentIcon = document.createElement("div");
        productCardContent.appendChild(cardContentIcon);
        cardContentIcon.classList.add("card-content_icon");

        let cardContentSVG = document.createElement("div");
        cardContentIcon.appendChild(cardContentSVG);
        cardContentSVG.classList.add("bear-svg-icon");
        cardContentSVG.innerHTML = `<svg class="bear-body" viewBox="0 0 512 512">
                                        <path d="M55.819,138.797c3.809-4.957,7.986-9.691,12.495-14.173C59.153,118.867,53,108.734,53,97.132   c0-17.939,14.596-32.535,32.534-32.535c16.039,0,29.313,11.682,31.977,26.966c5.73-2.5,11.633-4.744,17.732-6.652   c-5.505-22.359-25.669-39.019-49.709-39.019c-28.25,0-51.24,22.989-51.24,51.24C34.294,114.309,42.819,129.499,55.819,138.797z" fill="#fff"/>
                    <path d="M297.535,64.597c17.944,0,32.534,14.596,32.534,32.535c0,11.834-6.418,22.115-15.893,27.809   c4.518,4.521,8.683,9.292,12.494,14.292c13.336-9.258,22.104-24.667,22.104-42.094c0-28.257-22.987-51.24-51.24-51.24   c-24.132,0-44.372,16.791-49.779,39.299c6.113,1.946,12.038,4.217,17.774,6.753C268.033,76.474,281.374,64.597,297.535,64.597z" fill="#fff"/>
                    <path d="M11.505,213.173c0,86.782,80.666,157.392,179.818,157.392c99.147,0,179.811-70.609,179.811-157.392   c0-19.827-4.153-39.104-12.355-57.399c14.937-15.829,23.383-36.831,23.383-58.642c0-47.164-38.368-85.535-85.535-85.535   c-32.309,0-61.378,17.933-75.98,46.278c-19.497-2.801-39.573-2.789-59.107,0.073c-14.593-28.387-43.668-46.351-76.017-46.351   C38.371,11.597,0,49.968,0,97.132c0,21.991,8.567,43.123,23.705,58.985C15.606,174.312,11.505,193.475,11.505,213.173z    M18.706,97.132c0-36.852,29.98-66.829,66.829-66.829c27.249,0,51.523,16.316,61.851,41.571l2.868,7.021l7.468-1.367   c21.948-4.021,44.907-4.034,66.773-0.085l7.441,1.34l2.868-6.997c10.345-25.203,34.61-41.491,61.822-41.491   c36.852,0,66.829,29.98,66.829,66.832c0,18.867-8.117,36.97-22.244,49.669l-5.327,4.783l3.232,6.391   c5.529,10.952,9.354,22.329,11.448,33.996c1.339,7.143,2.07,14.462,2.07,21.927c0,38.052-18.183,72.486-47.544,97.378   c-29.168,25.051-69.398,40.566-113.768,40.566c-31.17,0-60.282-7.679-84.968-20.923c-46.092-24.21-76.821-67.553-76.821-117.021   c0-20.009,5.069-39.004,14.112-56.157l2.968-5.907l-5.376-4.771C26.917,134.352,18.706,116.157,18.706,97.132z" fill="#fff"/>
                    <path d="M75.459,258.598c0,43.951,51.853,79.719,115.597,79.719c63.738,0,115.594-35.768,115.594-79.719   c0-43.964-51.855-79.729-115.6-79.729C127.312,178.869,75.459,214.634,75.459,258.598z M191.05,249.244   c-14.636,0-25.982-6.99-25.982-12.994s11.347-12.981,25.982-12.981c14.629,0,25.979,6.978,25.979,12.981   S205.679,249.244,191.05,249.244z M191.05,197.572c53.423,0,96.894,27.377,96.894,61.025c0,31.651-38.489,57.737-87.538,60.715   v-52.038c20.417-2.978,35.33-15.533,35.33-31.024c0-17.768-19.626-31.688-44.686-31.688c-25.06,0-44.688,13.92-44.688,31.688   c0,15.491,14.916,28.047,35.335,31.024v52.038c-49.048-2.978-87.538-29.063-87.538-60.715   C94.166,224.949,137.624,197.572,191.05,197.572z" fill="#fff"/>
                    <path d="M122.462,157.271c-16.082,0-16.082,24.941,0,24.941S138.549,157.271,122.462,157.271z" fill="#fff"/>
                    <path d="M259.641,182.212c16.075,0,16.075-24.941,0-24.941C243.554,157.271,243.554,182.212,259.641,182.212z" fill="#fff"/>
                                    </svg>
                                    <svg class="bear-background" viewBox="0 0 512 512">
                                        <path stroke="none" d="M55.819,138.797c3.809-4.957,7.986-9.691,12.495-14.173C59.153,118.867,53,108.734,53,97.132   c0-17.939,14.596-32.535,32.534-32.535c16.039,0,29.313,11.682,31.977,26.966c5.73-2.5,11.633-4.744,17.732-6.652   c-5.505-22.359-25.669-39.019-49.709-39.019c-28.25,0-51.24,22.989-51.24,51.24C34.294,114.309,42.819,129.499,55.819,138.797z" fill="#fff"/>
                    <path d="M297.535,64.597c17.944,0,32.534,14.596,32.534,32.535c0,11.834-6.418,22.115-15.893,27.809   c4.518,4.521,8.683,9.292,12.494,14.292c13.336-9.258,22.104-24.667,22.104-42.094c0-28.257-22.987-51.24-51.24-51.24   c-24.132,0-44.372,16.791-49.779,39.299c6.113,1.946,12.038,4.217,17.774,6.753C268.033,76.474,281.374,64.597,297.535,64.597z" fill="#fff"/>
                    <path d="M11.505,213.173c0,86.782,80.666,157.392,179.818,157.392c99.147,0,179.811-70.609,179.811-157.392   c0-19.827-4.153-39.104-12.355-57.399c14.937-15.829,23.383-36.831,23.383-58.642c0-47.164-38.368-85.535-85.535-85.535   c-32.309,0-61.378,17.933-75.98,46.278c-19.497-2.801-39.573-2.789-59.107,0.073c-14.593-28.387-43.668-46.351-76.017-46.351   C38.371,11.597,0,49.968,0,97.132c0,21.991,8.567,43.123,23.705,58.985C15.606,174.312,11.505,193.475,11.505,213.173z    M18.706,97.132c0-36.852,29.98-66.829,66.829-66.829c27.249,0,51.523,16.316,61.851,41.571l2.868,7.021l7.468-1.367   c21.948-4.021,44.907-4.034,66.773-0.085l7.441,1.34l2.868-6.997c10.345-25.203,34.61-41.491,61.822-41.491   c36.852,0,66.829,29.98,66.829,66.832c0,18.867-8.117,36.97-22.244,49.669l-5.327,4.783l3.232,6.391   c5.529,10.952,9.354,22.329,11.448,33.996c1.339,7.143,2.07,14.462,2.07,21.927c0,38.052-18.183,72.486-47.544,97.378   c-29.168,25.051-69.398,40.566-113.768,40.566c-31.17,0-60.282-7.679-84.968-20.923c-46.092-24.21-76.821-67.553-76.821-117.021   c0-20.009,5.069-39.004,14.112-56.157l2.968-5.907l-5.376-4.771C26.917,134.352,18.706,116.157,18.706,97.132z" fill="#fff"/>
                    <path d="M75.459,258.598c0,43.951,51.853,79.719,115.597,79.719c63.738,0,115.594-35.768,115.594-79.719   c0-43.964-51.855-79.729-115.6-79.729C127.312,178.869,75.459,214.634,75.459,258.598z M191.05,249.244   c-14.636,0-25.982-6.99-25.982-12.994s11.347-12.981,25.982-12.981c14.629,0,25.979,6.978,25.979,12.981   S205.679,249.244,191.05,249.244z M191.05,197.572c53.423,0,96.894,27.377,96.894,61.025c0,31.651-38.489,57.737-87.538,60.715   v-52.038c20.417-2.978,35.33-15.533,35.33-31.024c0-17.768-19.626-31.688-44.686-31.688c-25.06,0-44.688,13.92-44.688,31.688   c0,15.491,14.916,28.047,35.335,31.024v52.038c-49.048-2.978-87.538-29.063-87.538-60.715   C94.166,224.949,137.624,197.572,191.05,197.572z" fill="#fff"/>
                    <path d="M122.462,157.271c-16.082,0-16.082,24.941,0,24.941S138.549,157.271,122.462,157.271z" fill="#fff"/>
                    <path d="M259.641,182.212c16.075,0,16.075-24.941,0-24.941C243.554,157.271,243.554,182.212,259.641,182.212z" fill="#fff"/>
                                        <!--bear bg Gradient-->
                                            <defs>
                                                <linearGradient id="bear_gradient" x2="0%" y2="60%">
                                                <stop offset="0%" stop-color="#9356DC" />
                                                <stop offset="100%" stop-color="#FF79DA" />
                                                </linearGradient>
                                            </defs>
                                            <!--bear bg Gradient-->
                                    </svg>`;

        // Format product price and convert cents to euros
        let cardContentPrice = document.createElement("div");
        productCardContent.appendChild(cardContentPrice);
        cardContentPrice.classList.add("card-content_price");

        item[i].price = item[i].price / 100;
        cardContentPrice.innerHTML = new Intl.NumberFormat("fr-FR", {
          style: "currency",
          currency: "EUR",
        }).format(item[i].price);

      }// End while
    })// End .then
} // End getItems();


