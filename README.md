# PierreGerardBlas_5_02062021

P5 - Orinoco - Build a e-commerce:

Installation:

Clone project:

git@gitlab.com:pgb_oc/orinoco.git

Installation command:

npm install

Start services commands:

node server

npm run sass

Project files infos:

host.js file switch auto local to server for the API url's


Gitlab Page Demo - Heroku server for backend:
https://pgb_oc.gitlab.io/orinoco

W3c validation for CSS URI:

index.html:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

product.html:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fproduct.html&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

cart.html:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fcart.html+&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

confirm.html:

https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fconfirm.html&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

W3c validation for HTML URI:

index.html:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2F

product.html:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fproduct.html

cart.html:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fcart.html

confirm.html:

https://validator.w3.org/nu/?doc=https%3A%2F%2Fpgb_oc.gitlab.io%2Forinoco%2Fconfirm.html